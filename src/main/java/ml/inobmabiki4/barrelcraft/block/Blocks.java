package ml.inobmabiki4.barrelcraft.block;

import ml.inobmabiki4.barrelcraft.BarrelcraftMod;

import net.minecraft.block.Block;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;


public class Blocks {
    private static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, BarrelcraftMod.MODID);

    public static void register() {
        REGISTRY.register(FMLJavaModLoadingContext.get().getModEventBus());
    }

    public static final RegistryObject<Block> RUM_BARREL = REGISTRY.register("rum_barrel", CaskBlock::new);
    public static final RegistryObject<Block> WINE_BARREL = REGISTRY.register("wine_barrel", CaskBlock::new);
    public static final RegistryObject<Block> BEER_BARREL = REGISTRY.register("beer_barrel", CaskBlock::new);
    public static final RegistryObject<Block> CIDER_BARREL = REGISTRY.register("cider_barrel", CaskBlock::new);
}
