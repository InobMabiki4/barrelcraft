package ml.inobmabiki4.barrelcraft.item;

import ml.inobmabiki4.barrelcraft.BarrelcraftMod;
import ml.inobmabiki4.barrelcraft.block.Blocks;
import ml.inobmabiki4.barrelcraft.block.CaskBlock;

import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;


public class Items {
    private static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, BarrelcraftMod.MODID);

    public static void register() {
        REGISTRY.register(FMLJavaModLoadingContext.get().getModEventBus());
    }

    public static final RegistryObject<Item> RUM_BARREL = REGISTRY.register("rum_barrel", () -> new CaskItem((CaskBlock) Blocks.RUM_BARREL.get()));
    public static final RegistryObject<Item> WINE_BARREL = REGISTRY.register("wine_barrel", () -> new CaskItem((CaskBlock) Blocks.WINE_BARREL.get()));
    public static final RegistryObject<Item> BEER_BARREL = REGISTRY.register("beer_barrel", () -> new CaskItem((CaskBlock) Blocks.BEER_BARREL.get()));
    public static final RegistryObject<Item> CIDER_BARREL = REGISTRY.register("cider_barrel", () -> new CaskItem((CaskBlock) Blocks.CIDER_BARREL.get()));

    public static final RegistryObject<Item> RUM_BOTTLE = REGISTRY.register("rum_bottle", () -> new HardDrinkItem(0.4F));
    public static final RegistryObject<Item> WINE_BOTTLE = REGISTRY.register("wine_bottle", () -> new HardDrinkItem(0.113F));
    public static final RegistryObject<Item> BEER_BOTTLE = REGISTRY.register("beer_bottle", () -> new HardDrinkItem(0.046F));
    public static final RegistryObject<Item> CIDER_BOTTLE = REGISTRY.register("cider_bottle", () -> new HardDrinkItem(0.045F));
}
