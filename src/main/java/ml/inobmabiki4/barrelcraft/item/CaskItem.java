package ml.inobmabiki4.barrelcraft.item;

import ml.inobmabiki4.barrelcraft.block.CaskBlock;

import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.Items;


public class CaskItem extends BlockItem {
    public CaskItem(CaskBlock block) {
        super(block, new Item.Properties()
                .group(ItemGroups.BARRELCRAFT)
                .containerItem(Items.BARREL)
        );
    }
}
