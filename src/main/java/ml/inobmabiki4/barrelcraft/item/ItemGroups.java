package ml.inobmabiki4.barrelcraft.item;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;


public class ItemGroups {
    public static final ItemGroup BARRELCRAFT = new ItemGroup("barrelcraft") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(Items.RUM_BARREL.get());
        }
    };
}
