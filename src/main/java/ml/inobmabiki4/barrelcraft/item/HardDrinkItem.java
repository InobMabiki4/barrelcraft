package ml.inobmabiki4.barrelcraft.item;

import net.minecraft.item.Food;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;


public class HardDrinkItem extends DrinkItem {
    public HardDrinkItem(float abv) {
        super(new Food.Builder()
                .hunger(0).saturation(1.2F)
                // Drinking is bad...
                .effect(() -> new EffectInstance(Effects.POISON), 0.1F * abv)
                .effect(() -> new EffectInstance(Effects.NAUSEA), 0.4F * abv)
                .effect(() -> new EffectInstance(Effects.WEAKNESS), 0.2F * abv)
                .effect(() -> new EffectInstance(Effects.SLOWNESS), 0.2F * abv)
                .effect(() -> new EffectInstance(Effects.MINING_FATIGUE), 0.2F * abv)
                // ...but sometimes it feels so good
                .effect(() -> new EffectInstance(Effects.LUCK), 0.1F * abv)
                .effect(() -> new EffectInstance(Effects.RESISTANCE), 0.1F * abv)
                .effect(() -> new EffectInstance(Effects.SLOW_FALLING), 0.1F * abv)
                .setAlwaysEdible()
                .build()
        );
    }
}
